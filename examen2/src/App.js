

import Header from './components/Header';
import Todos from './components/Todos';
import OnlyID from './components/OnlyID';
import OnlyIDTITLES from './components/OnlyIDTITLES';
import OnlyIDTITLEF from './components/OnlyIDTITLEF';
import OnlyIDTITLET from './components/OnlyIDTITLET';
import OnlyIDUSERID from './components/OnlyIDUSERID';
import OnlyIDUSERT from './components/OnlyIDUSERT';
import OnlyIDUSERF from './components/OnlyIDUSERF';


export default function App() {
  return (
    <div className="App">

     <Header></Header>


     <h1>Lista de todos los pendientes</h1>
     <Todos></Todos>
     <br></br>
     <h1>Lista de todos los pendientes - ID</h1>
     <OnlyID></OnlyID>
     <br></br>
     <h1>Lista de todos los pendientes - ID & TITLE </h1>
     <OnlyIDTITLES></OnlyIDTITLES>
     <br></br>
     <h1>Lista de todos los pendientes sin resolver - ID & TITLE</h1>
     <OnlyIDTITLEF></OnlyIDTITLEF>
     <br></br>
     <h1>Lista de todos los pendientes resueltos -ID & TITLE</h1>
     <OnlyIDTITLET></OnlyIDTITLET>
     <br></br>
     <h1>Lista de todos los pendientes - ID & USER ID</h1>
     <OnlyIDUSERID></OnlyIDUSERID>
     <br></br>
     <h1>Lista de todos los pendientes resueltos - ID & USER ID</h1>
     <OnlyIDUSERT></OnlyIDUSERT>
     <br></br>
     <h1>Lista de todos los pendientes sin resueltos - ID & USER ID</h1>
     <OnlyIDUSERF></OnlyIDUSERF>
    

    </div>
  );
}
