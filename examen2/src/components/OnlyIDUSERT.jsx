import React, { useState, useEffect } from 'react';
import '../App.css';

const url = "https://jsonplaceholder.typicode.com/todos"

export default function OnlyIDUSERT() {
  const [data, setData] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    fetch(url)
      .then(response => {
        if (!response.ok) {
          throw new Error('Failed to fetch data');
        }
        return response.json();
      })
      .then(result => {
        setIsLoading(false);
        setData(result);
      })
      .catch(error => {
        setIsLoading(false);
        setError(error);
      });
  }, []);

  const getContent = () => {
    if (isLoading) {
      return (
        <div className="App">
          <h4>Loading Data ...</h4>
          <progress value={null} />
        </div>
      );
    }

    if (error) {
      return <h4>Error: {error.message}</h4>;
    }


    const completedTodos = data.filter(todo => todo.completed);

    return (
      <div className="App">
        <div className='row'>
          <div className="col-2"></div>
          <div className="col-8">
            <table className="border-b border-blue-gray-100 bg-blue-gray-50 p-4">
              <thead className="border-b border-blue-gray-100 bg-blue-gray-50 p-4">
                <tr>
                  <th>ID</th>
                  <th>USER ID</th>
                </tr>
              </thead> 
              <tbody>
                {completedTodos.map(todo => (
                  <tr key={todo.id}>
                    <td className="border-b border-blue-gray-100 bg-blue-gray-50 p-4">{todo.id}</td>
                    <td className="border-b border-blue-gray-100 bg-blue-gray-50 p-4">{todo.userId}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
          <div className="col-2"></div>
        </div>
      </div>
    );
  }

  return (
    <div className="App">
      {getContent()}
    </div>
  );
}
